from django.db import models

# Create your models here.

class List(models.Model):
    name = models.CharField(max_length=50 , blank=False)

    def __str__(self):
        return self.name


class Item(models.Model):
    name = models.CharField(max_length=50)
    complete = models.BooleanField(default="False")
    price = models.FloatField(null=True, blank=True)
    count = models.CharField(max_length=10 ,null=True, blank=True)
    list = models.ForeignKey(to="List", on_delete=models.CASCADE)

    def __str__(self):
        return self.name