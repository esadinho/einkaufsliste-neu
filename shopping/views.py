from django.shortcuts import render, redirect, get_object_or_404
from . import models

# Create your views here.
def shopping(request, list_id):
    list = models.List.objects.all()
    context = {"lists":list}
    return render (request, "shopping/shopping.html", context)


def list(request, list_id):
    context = {}
    context["list"] = get_object_or_404(models.List, pk=list_id)
    context["item"] = models.Item.objects.filter(list=list_id)
    return rendirect(request, "shopping/list.html", context)

def add_list(request):
    models.List.objects.create(name=request.POST["list_name"])
    return redirect("shopping")

def add_item(request, list_id):
    list = get_object_or_404(models.List, pk=list_id)
    models.List.objects.create(name=request.POST["item_name"], list=list)
    return redirect("list", list_id)